#!/usr/bin/perl
sub VERSION_MESSAGE {
    print STDERR << "/";
vhd-change-parent.pl

Copyright (C) 2016 IGEL Co., Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/
}

sub HELP_MESSAGE {
    print STDERR << ".";
Usage: vhd-change-parent.pl [options]

  -o filename    target vhd file
  -p filename    vhd file that will be parent of target
  -f             skip consistency check

.
}

use strict;
use warnings;

use Encode::Locale;
use Encode;
use Fcntl;
use IO::Seekable;
use List::Util;
use Getopt::Std;
use File::Spec;

# Get arguments
$Getopt::Std::STANDARD_HELP_VERSION = 1;
our ($opt_o, $opt_p, $opt_f);

if (!(getopts ("o:p:f") and defined $opt_o and defined $opt_p)) {
    HELP_MESSAGE ();
    exit (1);
}

sub vhd_error {
    die Encode::encode (locale => $_[0]->{filename}).": ".$_[1];
}

sub vhd_open {
    my $ret = {};
    my ($filename, $uuid, $timestamp) = @_;
    $ret->{filename} = $filename;

    # Open
    sysopen ($ret->{file}, Encode::encode (locale_fs => $ret->{filename}),
	     O_RDONLY | O_BINARY)
	or vhd_error ($ret, "Open file: $!\n");

    # Read vhd footer
    sysseek ($ret->{file}, -512, SEEK_END) or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, $ret->{footer}, 512) == 512
	or vhd_error ($ret, "Read footer: $!");

    # Cookie
    if (substr ($ret->{footer}, 0, 8) ne "conectix") {
	vhd_error ($ret, "Incorrect footer cookie\n");
    }

    # Compare unique ID and time stamp
    if ($uuid and $timestamp and
	(substr ($ret->{footer}, 0x44, 16) ne $uuid or
	 substr ($ret->{footer}, 0x18, 4) ne $timestamp)) {
	vhd_error ($ret, "Unique ID and time stamp mismatch\n");
    }

    # Size
    $ret->{size} = unpack ("Q>", substr ($ret->{footer}, 0x30, 8));

    # Type
    my $vhd_type = unpack ("L>", substr ($ret->{footer}, 0x3C, 4));
    if ($vhd_type == 2) {
	$ret->{fixed} = 1;
	return ($ret);
    }
    if ($vhd_type != 3 and $vhd_type != 4) {
	vhd_error ($ret, "Invalid VHD type\n");
    }

    # Read header
    sysseek ($ret->{file}, unpack ("Q>", substr ($ret->{footer}, 0x10, 8)),
	     SEEK_SET) or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, $ret->{header}, 1024) == 1024
	or vhd_error ($ret, "Read header: $!");

    # Cookie
    if (substr ($ret->{header}, 0, 8) ne "cxsparse") {
	vhd_error ($ret, "Incorrect header cookie\n");
    }

    # Read BAT
    $ret->{block_size} = unpack ("L>", substr ($ret->{header}, 0x20, 4));
    $ret->{bat_offset} = unpack ("Q>", substr ($ret->{header}, 0x10, 8));
    $ret->{bat_entries} = unpack ("L>", substr ($ret->{header}, 0x1C, 4));
    sysseek ($ret->{file}, $ret->{bat_offset}, SEEK_SET)
	or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, $ret->{bat}, $ret->{bat_entries} * 4)
	== $ret->{bat_entries} * 4 or vhd_error ($ret, "Read BAT: $!");

    if ($vhd_type == 3) {
	$ret->{dynamic} = 1;
	return ($ret);
    }
    $ret->{differencing} = 1;

    # Parent Locator Entry
    my $found = -1;
    for (my $i = 0; $i < 8; $i++) {
	my ($p_code, $p_data_space, $p_data_length, $p_reserved,
	    $p_data_offset) =
		unpack ("L>4Q>", substr ($ret->{header}, 0x240 + 24 * $i, 24));
	next if ($p_code == 0);
	if ($p_code == 0x57327275) {
	    # W2ru (Windows relative pathname)
	    if ($found != -1) {
		vhd_error ($ret, "Multiple W2ru found\n");
	    }
	    $found = $i;
	}
    }
    if ($found == -1) {
	vhd_error ($ret, "No W2ru found\n");
    }
    my ($p_code, $p_data_space, $p_data_length, $p_reserved, $p_data_offset) =
	unpack ("L>4Q>", substr ($ret->{header}, 0x240 + 24 * $found, 24));
    sysseek ($ret->{file}, $p_data_offset, SEEK_SET)
	or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, my $p_data, $p_data_length) == $p_data_length
	or vhd_error ($ret, "Read parent locator: $!");
    my $p_filename = Encode::decode ("UTF-16LE", $p_data);
    if (substr ($p_filename, 0, 2) eq ".\\" or
	substr ($p_filename, 0, 2) eq "./") {
	$p_filename = substr ($p_filename, 2);
    }
    if (index ($p_filename, "\\") != -1 or index ($p_filename, "/") != -1) {
	vhd_error ($ret, "W2ru (Windows relative pathname) contains".
		   " directory name\n");
    }
    my @path = File::Spec->splitpath ($ret->{filename});
    $path[2] = $p_filename;
    my @parent = vhd_open (File::Spec->catpath (@path),
			   substr ($ret->{header}, 0x28, 16),
			   substr ($ret->{header}, 0x38, 4));
    if ($parent[0]->{size} != $ret->{size}) {
	vhd_error ($ret, "Size mismatch\n");
    }
    return ($ret, @parent);
}

# Open
my $vhd_new_parent_0;
my @vhd_list;
{
    my @vhd_target = vhd_open (Encode::decode (locale => $opt_o), 0, 0);
    if (!$vhd_target[0]->{differencing}) {
	die "Target VHD is not differencing.";
    }
    my @vhd_new_parent = vhd_open (Encode::decode (locale => $opt_p), 0, 0);
    $vhd_new_parent_0 = $vhd_new_parent[0];
    if ($opt_f) {
	@vhd_target = ($vhd_target[0]);
	@vhd_new_parent = ();
    } else {
	my $a = pop (@vhd_target);
	my $b = pop (@vhd_new_parent);
	if ($a->{footer} ne $b->{footer} or $a->{header} ne $b->{header}) {
	    die "Parent VHD file and target VHD file are unrelated.";
	}
	while (1) {
	    if ($#vhd_target < 0) {
		die "Parent VHD file is descendant of target VHD file.";
	    }
	    if ($#vhd_new_parent < 0) {
		# Target VHD file is descendant of parent VHD file.  Okay.
		last;
	    }
	    $a = pop (@vhd_target);
	    $b = pop (@vhd_new_parent);
	    if ($a->{footer} ne $b->{footer} or $a->{header} ne $b->{header}) {
		push (@vhd_target, $a);
		push (@vhd_new_parent, $b);
		last;
	    }
	}
    }
    @vhd_list = (@vhd_target, @vhd_new_parent);
}
if (length (Encode::encode ("UTF-16BE",
			    $vhd_new_parent_0->{filename})) > 512) {
    vhd_error ($vhd_new_parent_0, "Filename too long\n");
}

my $bat_entries;
{
    use integer;
    $bat_entries = ($vhd_list[0]->{size} + 0x1FFFFF) / 0x200000;
}

# Make sure every VHD has same block size...
my $block_size = 0x200000;
for (my $i = 0; $i < $#vhd_list; $i++) {
    if (!$vhd_list[$i]->{differencing}) {
	die;
    }
    if ($vhd_list[$i]->{block_size} != $block_size) {
	vhd_error ($vhd_list[$i], "Block size is not 2MiB\n");
    }
}

# Consistency check
for (my $block_number = 0; $block_number < $bat_entries; $block_number++) {
    if ((my $bitmap_lba =
	 unpack ("L>", substr ($vhd_list[0]->{bat}, $block_number * 4, 4))) !=
	0xFFFFFFFF) {
	sysseek ($vhd_list[0]->{file}, $bitmap_lba * 0x200, SEEK_SET)
	    or vhd_error ($vhd_list[0], "Seek: $!");
	sysread ($vhd_list[0]->{file}, my $bitmap, 0x200) == 0x200
	    or vhd_error ($vhd_list[0], "Read: $!");
	my $bitmap_bits = unpack ("B4096", $bitmap);
	my $regexp = "^".$bitmap_bits."\$";
	$regexp =~ s/0/./g;
	$regexp =~ s/1/0/g;
	for (my $i = 1; $i <= $#vhd_list; $i++) {
	    if ((my $bitmap_lba =
		 unpack ("L>",
			 substr ($vhd_list[$i]->{bat}, $block_number * 4, 4)))
		!= 0xFFFFFFFF) {
		sysseek ($vhd_list[$i]->{file}, $bitmap_lba * 0x200, SEEK_SET)
		    or vhd_error ($vhd_list[$i], "Seek: $!");
		sysread ($vhd_list[$i]->{file}, my $bitmap, 0x200) == 0x200
		    or vhd_error ($vhd_list[$i], "Read: $!");
		my $cmp_bitmap_bits = unpack ("B4096", $bitmap);
		if ($cmp_bitmap_bits !~ /$regexp/) {
		    vhd_error ($vhd_list[$i], "Consistency check failed".
			       " at block $block_number\n");
		}
	    }
	}
    }
}

# Parent Locator Entry
my $vhd_footer = $vhd_list[0]->{footer};
my $vhd_header = $vhd_list[0]->{header};
sysopen (my $vhd_file, Encode::encode (locale_fs => $vhd_list[0]->{filename}),
	 O_RDWR | O_BINARY)
    or vhd_error ($vhd_list[0], "Open file for writing: $!\n");
my $found = -1;
for (my $i = 0; $i < 8; $i++) {
    my ($p_code, $p_data_space, $p_data_length, $p_reserved, $p_data_offset) =
	unpack ("L>4Q>", substr ($vhd_header, 0x240 + 24 * $i, 24));
    next if ($p_code == 0);
    if ($p_code == 0x57327275) {
	# W2ru (Windows relative pathname)
	if ($found != -1) {
	    die "Multiple W2ru found";
	}
	$found = $i;
    }
}
if ($found == -1) {
    die "No W2ru found";
}
my ($p_code, $p_data_space, $p_data_length, $p_reserved, $p_data_offset) =
    unpack ("L>4Q>", substr ($vhd_header, 0x240 + 24 * $found, 24));
sysseek ($vhd_file, $p_data_offset, SEEK_SET) or die "Seek: $!";

# Modify
my @vhd_new_parent_path =
    File::Spec->splitpath ($vhd_new_parent_0->{filename});
my $new_data = Encode::encode ("UTF-16LE", $vhd_new_parent_path[2]);
$p_data_length = length ($new_data);
if ($p_data_length > $p_data_space) {
    # TODO: allocate space at footer and move footer and data
    die "Name too long";
}
substr ($vhd_header, 0x28, 16,	# UUID
	substr ($vhd_new_parent_0->{footer}, 0x44, 16));
substr ($vhd_header, 0x38, 4,	# Timestamp
	substr ($vhd_new_parent_0->{footer}, 0x18, 4));
substr ($vhd_header, 0x40, 512,	# Filename
	pack ("Z512", Encode::encode ("UTF-16BE",
				      $vhd_new_parent_0->{filename})));
substr ($vhd_header, 0x240 + 24 * $found, 24,
	pack ("L>4Q>", $p_code, $p_data_space, $p_data_length, $p_reserved,
	      $p_data_offset));
substr ($vhd_header, 0x24, 4, pack ("L>", 0));
substr ($vhd_header, 0x24, 4,
	pack ("L>", ~List::Util::sum (unpack ("C*", $vhd_header))));
syswrite ($vhd_file, $new_data, $p_data_length) == $p_data_length
    or die "Write parent locator: $!";
sysseek ($vhd_file, unpack ("Q>", substr ($vhd_footer, 0x10, 8)), SEEK_SET)
    or die "Seek: $!";
syswrite ($vhd_file, $vhd_header, 1024) == 1024 or die "Write header: $!";
