#!/usr/bin/perl
sub VERSION_MESSAGE {
    print STDERR << "/";
vhd-create.pl

Copyright (C) 2015-2016 IGEL Co., Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/
}

sub HELP_MESSAGE {
    print STDERR << "/";
Usage: vhd-create.pl [options] > output.vhd

  -i filename    copy from raw image or device
  -o filename    specify output file instead of STDOUT
  -s size        create an empty vhd with size bytes
  -f             create a fixed VHD

/
}

use strict;
use warnings;

use Encode::Locale;
use Encode;
use Fcntl;
use IO::Seekable;
use List::Util;
use Getopt::Std;

sub uuid_generate {
    my $ret = 0;
    if (!$ret) {
	# UUID is provided by libuuid-perl package in Debian.
	$ret = eval {
	    require UUID;
	    UUID::generate (my $tmp);
	    return $tmp;
	};
    }
    if (!$ret) {
	# Data::UUID is provided by libossp-uuid-perl package in
	# Debian and uuid-perl package in CentOS.
	$ret = eval {
	    require Data::UUID;
	    return Data::UUID->new->create;
	};
    }
    if (!$ret && `which uuidgen` ne "") {
	# uuidgen command is provided by uuid-runtime package in
	# Debian and util-linux package in CentOS 7.
	my $tmp = `uuidgen`;
	if (!$tmp) {
	    $tmp = "";
	}
	chomp ($tmp);
	if ($tmp =~ /^([0-9A-Fa-f]{8})-([0-9A-Fa-f]{4})-([0-9A-Fa-f]{4})-
                      ([0-9A-Fa-f]{4})-([0-9A-Fa-f]{12})$/sx) {
	    $ret = pack ("H32", $1.$2.$3.$4.$5);
	}
    }
    if (!$ret) {
	# python is installed by default on Debian and CentOS.
	# This code works with python2 and python3.
	my $tmp = `python -c 'import uuid;print(uuid.uuid4())'`;
	if (!$tmp) {
	    $tmp = "";
	}
	chomp ($tmp);
	if ($tmp =~ /^([0-9A-Fa-f]{8})-([0-9A-Fa-f]{4})-([0-9A-Fa-f]{4})-
                      ([0-9A-Fa-f]{4})-([0-9A-Fa-f]{12})$/sx) {
	    $ret = pack ("H32", $1.$2.$3.$4.$5);
	}
    }
    if (!$ret) {
	die "UUID, Data::UUID, uuidgen command or python required.\n";
    }
    return $ret;
}

sub create_vhd_footer {
    my ($opt_f, $input_size, $vhd_uuid) = @_;
    my ($vhd_sec, $vhd_head, $vhd_cyl);
    {
	use integer;
	my $cylxhead;
	my $vhd_totalsec = List::Util::min ($input_size / 512,
					    65535 * 16 * 255);
	if ($vhd_totalsec >= 65535 * 16 * 63) {
	    $vhd_sec = 255;
	    $vhd_head = 16;
	    $cylxhead = $vhd_totalsec / $vhd_sec;
	} else {
	    $vhd_sec = 17;
	    $cylxhead = $vhd_totalsec / 17;
	    $vhd_head = List::Util::max (($cylxhead + 1023) / 1024, 4);
	    if ($cylxhead >= $vhd_head * 1024 || $vhd_head > 16) {
		$vhd_sec = 31;
		$vhd_head = 16;
		$cylxhead = $vhd_totalsec / $vhd_sec;
	    }
	    if ($cylxhead >= $vhd_head * 1024) {
		$vhd_sec = 63;
		$vhd_head = 16;
		$cylxhead = $vhd_totalsec / $vhd_sec;
	    }
	}
	$vhd_cyl = $cylxhead / $vhd_head;
    }
    my $vhd_footer =
	"conectix".
	pack ("L>2Q>", 2, 0x00010000, $opt_f ? 2 ** 64 - 1 : 512).
	pack ("L>a4L>a4", time () - 946684800, "IGEL", 0x00010000, "Wi2k").
	pack ("Q>2", $input_size, $input_size).
	pack ("S>C2", $vhd_cyl, $vhd_head, $vhd_sec).
	pack ("L>", $opt_f ? 2 : 3).
	pack ("L>", 0).
	$vhd_uuid.
	pack ("C", 0).
	pack ("C*", (0) x 427);
    substr ($vhd_footer, 0x40, 4,
	    pack ("L>", ~List::Util::sum (unpack ("C*", $vhd_footer))));
    return $vhd_footer;
}

sub create_vhd_header {
    my ($vhd_bat_pos, $input_size) = @_;
    my $input_nblocks;
    {
	use integer;
	$input_nblocks = ($input_size + 0x1FFFFF) / 0x200000;
    }
    my $vhd_header =
	"cxsparse".
	pack ("L>8", 0xFFFFFFFF, 0xFFFFFFFF, 0, $vhd_bat_pos, 0x00010000,
	      $input_nblocks, 0x200000, 0).
	      pack ("L>S>2C8", (0) x 11).
	      pack ("L>", 0).
	      pack ("L>", 0).
	      pack ("C*", (0) x (512 + 24 * 8 + 256));
    substr ($vhd_header, 0x24, 4,
	    pack ("L>", ~List::Util::sum (unpack ("C*", $vhd_header))));
    return $vhd_header;
}

# Get arguments
$Getopt::Std::STANDARD_HELP_VERSION = 1;
our ($opt_i, $opt_o, $opt_s, $opt_f);

if (!(getopts ("i:o:s:f") and ((defined $opt_i && !defined $opt_s) or
			       (!defined $opt_i && defined $opt_s)))) {
    HELP_MESSAGE ();
    exit (1);
}

# Open the input
my ($input_file, $input_size);
my $pipe_mode = 0;
if (defined $opt_i) {
    if ($opt_i eq "-") {
	$input_file = \*STDIN;
    } else {
	sysopen ($input_file,
		 Encode::encode (locale_fs =>
				 Encode::decode (locale => $opt_i)),
		 O_RDONLY | O_BINARY) or die "Open input file: $!\n";
    }
    $input_size = sysseek ($input_file, 0, SEEK_END) or $pipe_mode = 1;
    if (!$pipe_mode) {
	sysseek ($input_file, 0, SEEK_SET) or die "Seek: $!";
	if ($input_size == 0) { # $input_size may be 0 but true
	    $pipe_mode = 1;
	}
    }
    if ($pipe_mode) {
	$input_size = 0;
    }
} else {
    $input_size = $opt_s - 0;
}

# Create vhd footer
my $vhd_uuid = pack ("L>S>2C8", unpack ("L<S<2C8", uuid_generate ()));
my $vhd_footer = create_vhd_footer ($opt_f, $input_size, $vhd_uuid);

# Create dynamic disk header
my $vhd_bat_pos = 512 + 1024;
my $vhd_header = create_vhd_header ($vhd_bat_pos, $input_size);
my $vhd_bat_len;
{
    use integer;
    my $input_nblocks = ($input_size + 0x1FFFFF) / 0x200000;
    if ($pipe_mode) {
	# The input size is unknown in pipe mode.  However the size of
	# BAT is needed before copying data.  1048576 is number of
	# blocks of maximum image size 2TiB.
	$input_nblocks = 1048576;
    }
    my $tmp = ($input_nblocks * 4 + 511) / 512;
    $vhd_bat_len = $tmp * 512;
}

# Create an empty vhd
my $output;
if (defined $opt_o) {
    sysopen ($output,
	     Encode::encode (locale_fs => Encode::decode (locale => $opt_o)),
	     O_WRONLY | O_CREAT | O_EXCL | O_BINARY)
	or die "Open output file: $!\n";
} else {
    $output = \*STDOUT;
    binmode ($output);
}
if ($opt_f) {
    if (defined $opt_i) {
	my $zero_block = "\x00" x 4096;
	my $total_readsize = 0;
	my $eof = 0;
	while (!$eof) {
	    my $readsize = 0;
	    my $readmax = 4096;
	    my $buf = "";
	    while ($readsize < $readmax) {
		my $readsize2 = sysread ($input_file, $buf,
					 $readmax - $readsize, $readsize);
		if (!defined $readsize2) {
		    die "Read: $!";
		}
		if ($readsize2 == 0) {
		    $eof = 1;
		    last;
		}
		$readsize += $readsize2;
	    }
	    if ($readsize == 0) {
		last;
	    }
	    $total_readsize += $readsize;
	    if ($buf ne $zero_block) {
		syswrite ($output, $buf, $readsize) == $readsize
		    or die "Write: $!";
	    } else {
		sysseek ($output, $readsize, SEEK_CUR) or die "Seek: $!";
	    }
	}
	if ($pipe_mode) {
	    $vhd_footer = create_vhd_footer ($opt_f, $total_readsize,
					     $vhd_uuid);
	}
    } else {
	sysseek ($output, $input_size, SEEK_SET) or die "Seek: $!";
    }
    syswrite ($output, $vhd_footer, 512) == 512 or die "Write: $!";
    exit (0);
}
syswrite ($output, $vhd_footer, 512) == 512 or die "Write: $!";
syswrite ($output, $vhd_header, 1024) == 1024 or die "Write: $!";
$vhd_bat_pos == 512 + 1024 or die;
syswrite ($output, "\xff" x $vhd_bat_len, $vhd_bat_len) == $vhd_bat_len
    or die "Write: $!";
if (($vhd_bat_pos + $vhd_bat_len + 512) % 4096 > 0) {
    my $align4k = 4096 - ($vhd_bat_pos + $vhd_bat_len + 512) % 4096;
    syswrite ($output, "\x00" x $align4k, $align4k) == $align4k
	or die "Write: $!";
}
syswrite ($output, $vhd_footer, 512) == 512 or die "Write: $!";
if (!defined $opt_i) {
    exit (0);
}
my $pos = sysseek ($output, -512, SEEK_CUR)
    or die "Seek error: $!. Redirect stdout to a file.\n";

# Convert
my $zero_block = "\x00" x 0x200000;
my $lba = 0;
my $total_readsize = 0;
my $eof = 0;
while (!$eof) {
    my $readsize = 0;
    my $readmax = 0x200000;
    my $buf = "";
    while ($readsize < $readmax) {
	my $readsize2 = sysread ($input_file, $buf,
				 $readmax - $readsize, $readsize);
	if (!defined $readsize2) {
	    die "Read: $!";
	}
	if ($readsize2 == 0) {
	    $eof = 1;
	    last;
	}
	$readsize += $readsize2;
    }
    if ($readsize == 0) {
	last;
    }
    $total_readsize += $readsize;
    my $nsec = $readsize >> 9;
    if ($readsize < 0x200000) {
	$zero_block = "\x00" x $readsize;
    }
    if ($pipe_mode) {
	$vhd_footer = create_vhd_footer ($opt_f, $total_readsize,
					 $vhd_uuid);
	$vhd_header = create_vhd_header ($vhd_bat_pos, $total_readsize);
	sysseek ($output, 0, SEEK_SET) or die "Seek: $!";
	syswrite ($output, $vhd_footer, 512) == 512 or die "Write: $!";
	syswrite ($output, $vhd_header, 1024) == 1024 or die "Write: $!";
    }
    if ($buf ne $zero_block) {
	my $block_offset = $pos;
	sysseek ($output, $vhd_bat_pos + 4 * ($lba >> 12), SEEK_SET)
	    or die "Seek: $!";
	syswrite ($output, pack ("L>", $block_offset >> 9), 4) == 4
	    or die "Write: $!";
	$pos += 512 + 0x200000 + 0xE00;
	sysseek ($output, $pos, SEEK_SET) or die "Seek: $!";
	syswrite ($output, $vhd_footer, 512) == 512 or die "Write: $!";
	sysseek ($output, $block_offset + 512, SEEK_SET) or die "Seek: $!";
	syswrite ($output, $buf, $readsize) == $readsize or die "Write: $!";
	my $bitmap = pack ("a512", ("\xff" x ($nsec >> 3)).
			   pack ("C", (0xFF >> ($nsec & 7)) ^ 0xFF));
	sysseek ($output, $block_offset, SEEK_SET) or die "Seek: $!";
	syswrite ($output, $bitmap, 512) == 512 or die "Write: $!";
    } elsif ($pipe_mode) {
	sysseek ($output, $pos, SEEK_SET) or die "Seek: $!";
	syswrite ($output, $vhd_footer, 512) == 512 or die "Write: $!";
    }
    $lba += $nsec;
    if ($readsize < 0x200000) {
	last;
    }
}
