#!/usr/bin/perl
sub VERSION_MESSAGE {
    print STDERR << "/";
vhd-info.pl

Copyright (C) 2015 IGEL Co., Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/
}

sub HELP_MESSAGE {
    print STDERR << "/";
Usage: vhd-info.pl input.vhd

/
}

use strict;
use warnings;

use Encode::Locale;
use Encode;
use Fcntl;
use IO::Seekable;
use List::Util;
use Getopt::Std;

# Get arguments
$Getopt::Std::STANDARD_HELP_VERSION = 1;
getopts ("") or die;
if (@ARGV != 1) {
    HELP_MESSAGE ();
    exit (1);
}
my $input_filename = $ARGV[0];

# Open the input
sysopen (my $input_file, $input_filename, O_RDONLY | O_BINARY)
    or die "Open input file: $!\n";

# Read vhd footer
sysseek ($input_file, -512, SEEK_END) or die "Seek: $!";
sysread ($input_file, my $vhd_footer, 512) == 512 or die "Read footer: $!";

# Cookie
if (substr ($vhd_footer, 0, 8) ne "conectix") {
    die "Incorrect footer cookie\n";
}

# Checksum
{
    my $checksum_data = substr ($vhd_footer, 0, 0x40).
	substr ($vhd_footer, 0x44);
    my $checksum = ~List::Util::sum (unpack ("C*", $checksum_data))
	& 0xFFFFFFFF;
    my $file_checksum = unpack ("L>", substr ($vhd_footer, 0x40, 4));

    if ($checksum != $file_checksum) {
	printf
	    STDERR "Incorrect footer checksum %08x should be %08x\n",
	    $file_checksum, $checksum;
    }
}

# Hard Disk Footer
sub print_footer {
    my $vhd_footer = $_[0];
    my $vhd_time = unpack ("L>", substr ($vhd_footer, 0x18, 4));
    my $vhd_type = unpack ("L>", substr ($vhd_footer, 0x3C, 4));
    my $vhd_type_str = "Unknown";
    if ($vhd_type == 0) {
	$vhd_type_str = "None";
    } elsif ($vhd_type == 2) {
	$vhd_type_str = "Fixed hard disk";
    } elsif ($vhd_type == 3) {
	$vhd_type_str = "Dynamic hard disk";
    } elsif ($vhd_type == 4) {
	$vhd_type_str = "Differencing hard disk";
    }
    my ($vhd_origsize, $vhd_cursize, $vhd_geom_c, $vhd_geom_h, $vhd_geom_s) =
	unpack ("Q>2S>C2", substr ($vhd_footer, 0x28, 20));
    printf
	"Features              %x\n".
	"File Format Version   %x\n".
	"Data Offset           %x\n".
	"Time Stamp            %x (%s)\n".
	"Creator Application   %x\n".
	"Creator Version       %x\n".
	"Creator Host OS       %x\n".
	"Original Size         %x (%u)\n".
	"Current Size          %x (%u)\n".
	"Disk Geometry CHS     %x %x %x (%u %u %u)\n".
	"Disk Type             %x (%s)\n".
	"Unique ID".
	"             %08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x\n".
	"Saved State           %x\n",
	unpack ("L>2Q>", substr ($vhd_footer, 0x8, 16)), $vhd_time,
	scalar (localtime ($vhd_time + 946684800)),
	unpack ("L>3", substr ($vhd_footer, 0x1C, 12)),
	$vhd_origsize, $vhd_origsize, $vhd_cursize, $vhd_cursize, $vhd_geom_c,
	$vhd_geom_h, $vhd_geom_s, $vhd_geom_c, $vhd_geom_h, $vhd_geom_s,
	$vhd_type, $vhd_type_str,
	unpack ("L<S<2C9", substr ($vhd_footer, 0x44, 17));
    if ($vhd_type != 3 and $vhd_type != 4) {
	exit (0);
    }
}
print "Hard Disk Footer:\n";
print_footer $vhd_footer;

# Read copy of footer
sysseek ($input_file, 0, SEEK_SET) or die "Seek: $!";
sysread ($input_file, my $vhd_footer_copy, 512) == 512
    or die "Read copy of footer: $!";
if ($vhd_footer ne $vhd_footer_copy) {
    print STDERR "Footer and copy of footer differ.\n";
    print "Copy of Hard Disk Footer:\n";
    print_footer $vhd_footer_copy;
}

# Read header
sysseek ($input_file, unpack ("Q>", substr ($vhd_footer, 0x10, 8)), SEEK_SET)
    or die "Seek: $!";
sysread ($input_file, my $vhd_header, 1024) == 1024 or die "Read header: $!";

# Cookie
if (substr ($vhd_header, 0, 8) ne "cxsparse") {
    die "Incorrect header cookie\n";
}

# Checksum
{
    my $checksum_data = substr ($vhd_header, 0, 0x24).
	substr ($vhd_header, 0x28);
    my $checksum = ~List::Util::sum (unpack ("C*", $checksum_data))
	& 0xFFFFFFFF;
    my $file_checksum = unpack ("L>", substr ($vhd_header, 0x24, 4));

    if ($checksum != $file_checksum) {
	printf STDERR "Incorrect header checksum %08x should be %08x\n",
	$file_checksum, $checksum;
    }
}

# Dynamic Disk Header
my $vhd_parent_time = unpack ("L>", substr ($vhd_header, 0x38, 4));
my $vhd_nent = unpack ("L>", substr ($vhd_header, 0x1C, 4));
my $vhd_blocksize = unpack ("L>", substr ($vhd_header, 0x20, 4));
printf "Dynamic Disk Header:\n".
    "Data Offset           %x\n".
    "Table Offset          %x\n".
    "Header Version        %x\n".
    "Max Table Entries     %x (%u) (%u bytes)\n".
    "Block Size            %x (%u)\n".
    "Parent Unique ID      %08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x\n".
    "Parent Time Stamp     %x (%s)\n".
    "Parent Unicode Name   %s\n",
    unpack ("Q>2L>", substr ($vhd_header, 0x8, 20)),
    $vhd_nent, $vhd_nent, $vhd_nent * $vhd_blocksize,
    $vhd_blocksize, $vhd_blocksize,
    unpack ("L<S<2C8", substr ($vhd_header, 0x28, 16)),
    $vhd_parent_time, scalar (localtime ($vhd_parent_time + 946684800)),
    Encode::encode (locale => 
		    unpack ("Z*",
			    Encode::decode ("UTF-16BE",
					    substr ($vhd_header, 0x40, 512))));
for (my $i = 0; $i < 8; $i++) {
    my ($p_code, $p_data_space, $p_data_length, $p_reserved, $p_data_offset) =
	unpack ("L>4Q>", substr ($vhd_header, 0x240 + 24 * $i, 24));
    next if ($p_code == 0);
    my $p_code_str = sprintf ("Unknown (%x)", $p_code);
    if ($p_code == 0x57693272) {
	$p_code_str = "Wi2r";
    } elsif ($p_code == 0x5769326B) {
	$p_code_str = "Wi2k";
    } elsif ($p_code == 0x57327275) {
	$p_code_str = "W2ru (Windows relative pathname)";
    } elsif ($p_code == 0x57326B75) {
	$p_code_str = "W2ku (Windows absolute pathname)";
    } elsif ($p_code == 0x4D616320) {
	$p_code_str = "Mac";
    } elsif ($p_code == 0x4D616358) {
	$p_code_str = "MacX";
    }
    printf "Parent Locator Entry %u:\n".
	"Platform Code         %s\n".
	"Platform Data Space   %x (%u)\n".
	"Platform Data Length  %x (%u)\n".
	"Platform Data Offset  %x\n",
	$i + 1, $p_code_str, $p_data_space, $p_data_space,
	$p_data_length, $p_data_length, $p_data_offset;
    if ($p_code == 0x57327275 or $p_code == 0x57326B75) {
	sysseek ($input_file, $p_data_offset, SEEK_SET) or die "Seek: $!";
	sysread ($input_file, my $p_data, $p_data_length) == $p_data_length
	    or die "Read parent locator: $!";
	printf
	    "Platform Data         %s\n",
	    Encode::encode (locale =>
			    Encode::decode ("UTF-16LE", $p_data));
    }
}
