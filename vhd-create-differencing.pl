#!/usr/bin/perl
sub VERSION_MESSAGE {
    print STDERR << "/";
vhd-create-differencing.pl

Copyright (C) 2015 IGEL Co., Ltd.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

/
}

sub HELP_MESSAGE {
    print STDERR << ".";
Usage: vhd-create-differencing.pl [options]

  -i filename    copy from raw image or device
  -p filename    parent vhd file
  -o filename    output differencing vhd file

.
}

use strict;
use warnings;

use Encode::Locale;
use Encode;
use Fcntl;
use IO::Seekable;
use List::Util;
use Getopt::Std;
use File::Spec;

sub uuid_generate {
    my $ret = 0;
    if (!$ret) {
	# UUID is provided by libuuid-perl package in Debian.
	$ret = eval {
	    require UUID;
	    UUID::generate (my $tmp);
	    return $tmp;
	};
    }
    if (!$ret) {
	# Data::UUID is provided by libossp-uuid-perl package in
	# Debian and uuid-perl package in CentOS.
	$ret = eval {
	    require Data::UUID;
	    return Data::UUID->new->create;
	};
    }
    if (!$ret && `which uuidgen` ne "") {
	# uuidgen command is provided by uuid-runtime package in
	# Debian and util-linux package in CentOS 7.
	my $tmp = `uuidgen`;
	if (!$tmp) {
	    $tmp = "";
	}
	chomp ($tmp);
	if ($tmp =~ /^([0-9A-Fa-f]{8})-([0-9A-Fa-f]{4})-([0-9A-Fa-f]{4})-
                      ([0-9A-Fa-f]{4})-([0-9A-Fa-f]{12})$/sx) {
	    $ret = pack ("H32", $1.$2.$3.$4.$5);
	}
    }
    if (!$ret) {
	# python is installed by default on Debian and CentOS.
	# This code works with python2 and python3.
	my $tmp = `python -c 'import uuid;print(uuid.uuid4())'`;
	if (!$tmp) {
	    $tmp = "";
	}
	chomp ($tmp);
	if ($tmp =~ /^([0-9A-Fa-f]{8})-([0-9A-Fa-f]{4})-([0-9A-Fa-f]{4})-
                      ([0-9A-Fa-f]{4})-([0-9A-Fa-f]{12})$/sx) {
	    $ret = pack ("H32", $1.$2.$3.$4.$5);
	}
    }
    if (!$ret) {
	die "UUID, Data::UUID, uuidgen command or python required.\n";
    }
    return $ret;
}

# Get arguments
$Getopt::Std::STANDARD_HELP_VERSION = 1;
our ($opt_i, $opt_p, $opt_o);

if (!(getopts ("i:p:o:") and defined $opt_i and defined $opt_p and
      defined $opt_o)) {
    HELP_MESSAGE ();
    exit (1);
}

my $cmp_mode = 1;

sub vhd_error {
    die Encode::encode (locale => $_[0]->{filename}).": ".$_[1];
}

sub vhd_open {
    my $ret = {};
    my ($filename, $uuid, $timestamp) = @_;
    $ret->{filename} = $filename;

    # Open
    sysopen ($ret->{file}, Encode::encode (locale_fs => $ret->{filename}),
	     O_RDONLY | O_BINARY)
	or vhd_error ($ret, "Open file: $!\n");

    # Read vhd footer
    sysseek ($ret->{file}, -512, SEEK_END) or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, $ret->{footer}, 512) == 512
	or vhd_error ($ret, "Read footer: $!");

    # Cookie
    if (substr ($ret->{footer}, 0, 8) ne "conectix") {
	vhd_error ($ret, "Incorrect footer cookie\n");
    }

    # Compare unique ID and time stamp
    if ($uuid and $timestamp and
	(substr ($ret->{footer}, 0x44, 16) ne $uuid or
	 substr ($ret->{footer}, 0x18, 4) ne $timestamp)) {
	vhd_error ($ret, "Unique ID and time stamp mismatch\n");
    }

    # Size
    $ret->{size} = unpack ("Q>", substr ($ret->{footer}, 0x30, 8));

    # Type
    my $vhd_type = unpack ("L>", substr ($ret->{footer}, 0x3C, 4));
    if ($vhd_type == 2) {
	$ret->{fixed} = 1;
	return ($ret);
    }
    if ($vhd_type != 3 and $vhd_type != 4) {
	vhd_error ($ret, "Invalid VHD type\n");
    }

    # Read header
    sysseek ($ret->{file}, unpack ("Q>", substr ($ret->{footer}, 0x10, 8)),
	     SEEK_SET) or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, $ret->{header}, 1024) == 1024
	or vhd_error ($ret, "Read header: $!");

    # Cookie
    if (substr ($ret->{header}, 0, 8) ne "cxsparse") {
	vhd_error ($ret, "Incorrect header cookie\n");
    }

    # Read BAT
    $ret->{block_size} = unpack ("L>", substr ($ret->{header}, 0x20, 4));
    $ret->{bat_offset} = unpack ("Q>", substr ($ret->{header}, 0x10, 8));
    $ret->{bat_entries} = unpack ("L>", substr ($ret->{header}, 0x1C, 4));
    sysseek ($ret->{file}, $ret->{bat_offset}, SEEK_SET)
	or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, $ret->{bat}, $ret->{bat_entries} * 4)
	== $ret->{bat_entries} * 4 or vhd_error ($ret, "Read BAT: $!");

    if ($vhd_type == 3) {
	$ret->{dynamic} = 1;
	return ($ret);
    }
    $ret->{differencing} = 1;

    # Parent Locator Entry
    my $found = -1;
    for (my $i = 0; $i < 8; $i++) {
	my ($p_code, $p_data_space, $p_data_length, $p_reserved,
	    $p_data_offset) =
		unpack ("L>4Q>", substr ($ret->{header}, 0x240 + 24 * $i, 24));
	next if ($p_code == 0);
	if ($p_code == 0x57327275) {
	    # W2ru (Windows relative pathname)
	    if ($found != -1) {
		vhd_error ($ret, "Multiple W2ru found\n");
	    }
	    $found = $i;
	}
    }
    if ($found == -1) {
	vhd_error ($ret, "No W2ru found\n");
    }
    my ($p_code, $p_data_space, $p_data_length, $p_reserved, $p_data_offset) =
	unpack ("L>4Q>", substr ($ret->{header}, 0x240 + 24 * $found, 24));
    sysseek ($ret->{file}, $p_data_offset, SEEK_SET)
	or vhd_error ($ret, "Seek: $!");
    sysread ($ret->{file}, my $p_data, $p_data_length) == $p_data_length
	or vhd_error ($ret, "Read parent locator: $!");
    my $p_filename = Encode::decode ("UTF-16LE", $p_data);
    if (substr ($p_filename, 0, 2) eq ".\\" or
	substr ($p_filename, 0, 2) eq "./") {
	$p_filename = substr ($p_filename, 2);
    }
    if (index ($p_filename, "\\") != -1 or index ($p_filename, "/") != -1) {
	vhd_error ($ret, "W2ru (Windows relative pathname) contains".
		   " directory name\n");
    }
    my @path = File::Spec->splitpath ($ret->{filename});
    $path[2] = $p_filename;
    my @parent = vhd_open (File::Spec->catpath (@path),
			   substr ($ret->{header}, 0x28, 16),
			   substr ($ret->{header}, 0x38, 4));
    if ($parent[0]->{size} != $ret->{size}) {
	vhd_error ($ret, "Size mismatch\n");
    }
    return ($ret, @parent);
}

sub vhd_read {
    my ($offset, $length, $ret, @arg) = @_;
    use integer;

    if (!$ret) {
	return "\x00" x $length;
    }
    if ($offset >= $ret->{size} or $offset + $length > $ret->{size}) {
	vhd_error ($ret, "Offset ".$offset." Size ".$length." VHD Size ".
		   $ret->{size});
    }
    if ($ret->{fixed}) {
	sysseek ($ret->{file}, $offset, SEEK_SET)
	    or vhd_error ($ret, "Seek: $!");
	sysread ($ret->{file}, my $buf, $length) == $length
	    or vhd_error ($ret, "Read: $!");
	return $buf;
    }
    my $buf = "";
    while ($length > 0) {
	my $block_number = $offset / $ret->{block_size};
	my $block_offset = $offset % $ret->{block_size};
	my $block_remain = $ret->{block_size} - $block_offset;
	if ($block_remain > $length) {
	    $block_remain = $length;
	}
	if ($block_number >= $ret->{bat_entries}) {
	    vhd_error ($ret, "Block number ".$block_number.
		" Max Table Entries ".$ret->{bat_entries});
	}
	my $bitmap_lba =
	    unpack ("L>", substr ($ret->{bat}, $block_number * 4, 4));
	if ($bitmap_lba == 0xFFFFFFFF) {
	    $buf .= vhd_read ($offset, $block_remain, @arg);
	    $offset += $block_remain;
	    $length -= $block_remain;
	} else {
	    sysseek ($ret->{file}, $bitmap_lba * 0x200, SEEK_SET)
		or vhd_error ($ret, "Seek: $!");
	    sysread ($ret->{file}, my $bitmap, 0x200) == 0x200
		or vhd_error ($ret, "Read: $!");
	    my $bitmap_bits = unpack ("B4096", $bitmap);
	    while ($block_remain > 0) {
		my $tmp = substr ($bitmap_bits, $block_offset / 0x200);
		my $mode;
		if ($tmp =~ /^(0+)/) {
		    $mode = 0;
		} elsif ($tmp =~ /^(1+)/) {
		    $mode = 1;
		} else {
		    die;
		}
		my $part_len = length ($1) * 0x200 - $block_offset % 0x200;
		if ($part_len > $block_remain) {
		    $part_len = $block_remain;
		}
		if ($mode) {
		    sysseek ($ret->{file}, $bitmap_lba * 0x200 + 0x200 +
			     $block_offset, SEEK_SET)
			or vhd_error ($ret, "Seek: $!");
		    sysread ($ret->{file}, my $buf_part, $part_len) ==
			$part_len or vhd_error ($ret, "Read: $!");
		    $buf .= $buf_part;
		} else {
		    $buf .= vhd_read ($offset, $part_len, @arg);
		}
		$offset += $part_len;
		$length -= $part_len;
		$block_offset += $part_len;
		$block_remain -= $part_len;
	    }
	}
    }
    return $buf;
}

sub vhd_write {
    my ($offset, $length, $buf, $ret, @arg) = @_;
    use integer;

    $ret or die;
    $ret->{differencing} or die;
    if ($offset >= $ret->{size} or $offset + $length > $ret->{size}) {
	vhd_error ($ret, "Offset ".$offset." Size ".$length." VHD Size ".
	    $ret->{size});
    }
    if ($offset % 0x200) {
	$buf = vhd_read ($offset - $offset % 0x200, $offset % 0x200, $ret,
			 @arg).$buf;
	$length += $offset % 0x200;
	$offset -= $offset % 0x200;
    }
    if ($length % 0x200) {
	$buf .= vhd_read ($offset + $length, 0x200 - $length % 0x200, $ret,
			  @arg);
	$length += 0x200 - $length % 0x200;
    }
    while ($length > 0) {
	my $block_number = $offset / $ret->{block_size};
	my $block_offset = $offset % $ret->{block_size};
	my $block_remain = $ret->{block_size} - $block_offset;
	if ($block_remain > $length) {
	    $block_remain = $length;
	}
	if ($block_number >= $ret->{bat_entries}) {
	    vhd_error ($ret, "Block number ".$block_number.
		" Max Table Entries ".$ret->{bat_entries});
	}
	my $bitmap_lba =
	    unpack ("L>", substr ($ret->{bat}, $block_number * 4, 4));
	my $bitmap;
	if ($bitmap_lba != 0xFFFFFFFF or !$cmp_mode or
	    vhd_read ($offset, $block_remain, $ret, @arg) ne
	    substr ($buf, 0, $block_remain)) {
	    if ($bitmap_lba == 0xFFFFFFFF) {
		my $pos = sysseek ($ret->{file}, -512, SEEK_END)
		    or die "Seek: $!";
		$bitmap_lba = $pos / 0x200;
		substr ($ret->{bat}, $block_number * 4, 4,
			pack ("L>", $bitmap_lba));
		sysseek ($ret->{file}, $ret->{bat_offset} + $block_number * 4,
			 SEEK_SET) or vhd_error ($ret, "Seek: $!");
		syswrite ($ret->{file}, pack ("L>", $bitmap_lba), 4) == 4
		    or vhd_error ($ret, "Write: $!");
		$pos += 512 + 0x200000 + 0xE00;
		sysseek ($ret->{file}, $pos, SEEK_SET)
		    or vhd_error ($ret, "Seek: $!");
		syswrite ($ret->{file}, $ret->{footer}, 512) == 512
		    or vhd_error ($ret, "Write: $!");
		$bitmap = "\x00" x 0x200;
	    } else {
		sysseek ($ret->{file}, $bitmap_lba * 0x200, SEEK_SET)
		    or vhd_error ($ret, "Seek: $!");
		sysread ($ret->{file}, $bitmap, 0x200) == 0x200
		    or vhd_error ($ret, "Read: $!");
	    }
	    my $bitmap_bits = unpack ("B4096", $bitmap);
	    if (substr ($bitmap_bits, $block_offset / 0x200,
			$block_remain / 0x200) =~ /0/) {
		substr ($bitmap_bits, $block_offset / 0x200,
			$block_remain / 0x200,
			"1" x ($block_remain / 0x200));
		$bitmap = pack ("B4096", $bitmap_bits);
		sysseek ($ret->{file}, $bitmap_lba * 0x200, SEEK_SET)
		    or vhd_error ($ret, "Seek: $!");
		syswrite ($ret->{file}, $bitmap, 0x200) == 0x200
		    or vhd_error ($ret, "Write: $!");
	    }
	    sysseek ($ret->{file}, $bitmap_lba * 0x200 + 0x200 + $block_offset,
		     SEEK_SET) or vhd_error ($ret, "Seek: $!");
	    syswrite ($ret->{file}, $buf, $block_remain) == $block_remain
		or vhd_error ($ret, "Write: $!");
	}
	$buf = substr ($buf, $block_remain);
	$offset += $block_remain;
	$length -= $block_remain;
    }
}

# Open
my @vhd = vhd_open (Encode::decode (locale => $opt_p), 0, 0);

my $bat_entries;
{
    use integer;
    $bat_entries = ($vhd[0]->{size} + 0x1FFFFF) / 0x200000;
}

# Create vhd footer
my ($vhd_sec, $vhd_head, $vhd_cyl);
{
    use integer;
    my $cylxhead;
    my $vhd_totalsec = List::Util::min ($vhd[0]->{size} / 512,
					65535 * 16 * 255);
    if ($vhd_totalsec >= 65535 * 16 * 63) {
	$vhd_sec = 255;
	$vhd_head = 16;
	$cylxhead = $vhd_totalsec / $vhd_sec;
    } else {
	$vhd_sec = 17;
	$cylxhead = $vhd_totalsec / 17;
	$vhd_head = List::Util::max (($cylxhead + 1023) / 1024, 4);
	if ($cylxhead >= $vhd_head * 1024 || $vhd_head > 16) {
	    $vhd_sec = 31;
	    $vhd_head = 16;
	    $cylxhead = $vhd_totalsec / $vhd_sec;
	}
	if ($cylxhead >= $vhd_head * 1024) {
	    $vhd_sec = 63;
	    $vhd_head = 16;
	    $cylxhead = $vhd_totalsec / $vhd_sec;
	}
    }
    $vhd_cyl = $cylxhead / $vhd_head;
}
my $vhd_uuid = pack ("L>S>2C8", unpack ("L<S<2C8", uuid_generate ()));
my $vhd_footer =
    "conectix".
    pack ("L>2Q>", 2, 0x00010000, 512).
    pack ("L>a4L>a4", time () - 946684800, "IGEL", 0x00010000, "Wi2k").
    pack ("Q>2S>C2", $vhd[0]->{size}, $vhd[0]->{size}, $vhd_cyl, $vhd_head,
	  $vhd_sec).
    pack ("L>", 4).
    pack ("L>", 0).
    $vhd_uuid.
    pack ("C", 0).
    pack ("C*", (0) x 427);
substr ($vhd_footer, 0x40, 4,
	pack ("L>", ~List::Util::sum (unpack ("C*", $vhd_footer))));

# Create dynamic disk header
if (length (Encode::encode ("UTF-16BE", $vhd[0]->{filename})) > 512) {
    vhd_error ($vhd[0], "Filename too long\n");
}
my $vhd_bat_pos = 512 + 1024 + 512;
my @vhd_0_path = File::Spec->splitpath ($vhd[0]->{filename});
my $vhd_header =
    "cxsparse".
    pack ("L>8", 0xFFFFFFFF, 0xFFFFFFFF, 0, $vhd_bat_pos, 0x00010000,
	  $bat_entries, 0x200000, 0).
    substr ($vhd[0]->{footer}, 0x44, 16). # UUID
    substr ($vhd[0]->{footer}, 0x18, 4).  # Timestamp
    pack ("L>", 0).
    pack ("Z512", Encode::encode ("UTF-16BE", $vhd[0]->{filename})).
    pack ("a4L>3Q>", "W2ru", 0x200,
	  length (Encode::encode ("UTF-16LE", $vhd_0_path[2])), 0, 0x600).
    pack ("C*", (0) x (24 * 7 + 256));
substr ($vhd_header, 0x24, 4,
	pack ("L>", ~List::Util::sum (unpack ("C*", $vhd_header))));
my $vhd_bat_len;
{
    use integer;
    my $tmp = ($bat_entries * 4 + 511) / 512;
    $vhd_bat_len = $tmp * 512;
}
my $vhd_parent_locator =
    pack ("Z512", Encode::encode ("UTF-16LE", $vhd_0_path[2]));

# Open the input
my ($input_file, $input_size);
my $pipe_mode = 0;
if (defined $opt_i) {
    if ($opt_i eq "-") {
	$input_file = \*STDIN;
    } else {
	sysopen ($input_file,
		 Encode::encode (locale_fs =>
				 Encode::decode (locale => $opt_i)),
		 O_RDONLY | O_BINARY) or die "Open input file: $!\n";
    }
    $input_size = sysseek ($input_file, 0, SEEK_END) or $pipe_mode = 1;
    if (!$pipe_mode) {
	sysseek ($input_file, 0, SEEK_SET) or die "Seek: $!";
	if ($input_size == 0) { # $input_size may be 0 but true
	    $pipe_mode = 1;
	}
    }
    if ($pipe_mode) {
	$input_size = 0;
    }
}

if ($input_size > 0 && $input_size != $vhd[0]->{size}) {
    die "Input file size ".$input_size.
	" is different from vhd size ".$vhd[0]->{size};
}

# Create an empty vhd
if (defined $opt_o) {
    unshift (@vhd,
	     {
		 filename => Encode::decode (locale => $opt_o),
		 footer => $vhd_footer,
		 size => $vhd[0]->{size},
		 header => $vhd_header,
		 block_size => 0x200000,
		 bat_offset => $vhd_bat_pos,
		 bat_entries => $bat_entries,
		 bat => "\xff" x $vhd_bat_len,
		 differencing => 1,
	     });
    sysopen ($vhd[0]->{file},
	     Encode::encode (locale_fs => $vhd[0]->{filename}),
	     O_RDWR | O_CREAT | O_EXCL | O_BINARY)
	or vhd_error ($vhd[0], "Open: $!\n");
    syswrite ($vhd[0]->{file}, $vhd_footer, 512) == 512
	or vhd_error ($vhd[0], "Write: $!");
    syswrite ($vhd[0]->{file}, $vhd_header, 1024) == 1024
	or vhd_error ($vhd[0], "Write: $!");
    syswrite ($vhd[0]->{file}, $vhd_parent_locator, 512) == 512
	or vhd_error ($vhd[0], "Write: $!");
    $vhd_bat_pos == 512 + 1024 + 512 or die;
    syswrite ($vhd[0]->{file}, "\xff" x $vhd_bat_len, $vhd_bat_len) ==
	$vhd_bat_len or vhd_error ($vhd[0], "Write: $!");
    if (($vhd_bat_pos + $vhd_bat_len + 512) % 4096 > 0) {
	my $align4k = 4096 - ($vhd_bat_pos + $vhd_bat_len + 512) % 4096;
	syswrite ($vhd[0]->{file}, "\x00" x $align4k, $align4k) == $align4k
	    or vhd_error ($vhd[0], "Write: $!");
    }
    syswrite ($vhd[0]->{file}, $vhd_footer, 512) == 512
	or vhd_error ($vhd[0], "Write: $!");
}

my $total_readsize = 0;
while ($total_readsize != $vhd[0]->{size}) {
    my $blocksize = $vhd[0]->{size} - $total_readsize;
    if ($blocksize > 0x200000) {
	$blocksize = 0x200000;
    }
    my $buf = "";
    my $readsize = 0;
    while ($readsize < $blocksize) {
	my $readsize2 = sysread ($input_file, $buf, $blocksize - $readsize,
				 $readsize);
	if (!defined $readsize2) {
	    die "Read: $!";
	}
	if ($readsize2 == 0) {
	    die "End of input file";
	}
	$readsize += $readsize2;
    }
    vhd_write ($total_readsize, $readsize, $buf, @vhd);
    $total_readsize += $readsize;
}
